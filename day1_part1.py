def read_integers(filename):
    with open(filename) as f:
        return [int(x) for x in f]

numbers = read_integers('day1_data.txt')
result = sum(numbers)
print(result)
