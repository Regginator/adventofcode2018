def read_integers(filename):
    with open(filename) as f:
        return [int(x) for x in f]

numbers = read_integers('day1_data.txt')

frequency = 0
frequencies = set([0])
find = True
while find:
    for x in numbers:
        frequency+=x
        if frequency in frequencies:
            print(frequency)
            find = False
            break
        else:
            frequencies.add(frequency)    